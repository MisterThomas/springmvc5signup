package net.javaguides.springmvc.signup.model;

public class SignUpForm {
    private String firstName;
    private String lastName;
    private String email;
    private String userName;
    private String password;


    public String getFirstName() {
        return firstName;
    }



    public String getLastName() {
        return lastName;
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }



    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
